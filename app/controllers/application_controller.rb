class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def hello
    s = 0
    n = 20
    #STDIN.gets.to_i
    for i in 0..n
       s = s + i.to_i 
       puts "Value of local variable is #{i}"
    end
    puts "Value of sum is #{s}" 
    #+ s.to_s
    render text: "hello, world! <br/>The sum of 1..#{n} is: #{s}"
    
  end
  def goodbye
    n = rand(1..100)
    s = n*(n+1)/2
    render text: "Goodbye, world <br/>The sum of 1..#{n} is: #{s}"
  end
end
